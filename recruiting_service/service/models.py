from django.db import models


class Planet(models.Model):
    name = models.CharField(max_length=200)
    objects = models.Manager()

    def __str__(self):
        return self.name


class Sith(models.Model):
    name = models.CharField(max_length=200)
    planet_of_teaching = models.ForeignKey(Planet, on_delete=models.CASCADE)
    objects = models.Manager()

    def __str__(self):
        return self.name


class Recruit(models.Model):
    name = models.CharField(max_length=200)
    planet_of_living = models.ForeignKey(Planet, on_delete=models.CASCADE)
    age = models.IntegerField()
    email = models.EmailField()
    accepted = models.BooleanField(default=False)
    sith = models.ForeignKey(Sith, on_delete=models.CASCADE, blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.name


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    answer = models.BooleanField(default=False)
    objects = models.Manager()

    def __str__(self):
        return self.question_text


class Result(models.Model):
    recruit = models.ForeignKey(Recruit, on_delete=models.CASCADE, related_name='recruit')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='question')
    result = models.BooleanField('Результат', default=False)
    objects = models.Manager()
