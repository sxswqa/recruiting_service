from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('recruit/', views.add_recruit, name='recruit'),
    path('test/<int:recruit_id>', views.test_recruit, name='test'),
    path('sith/', views.list_sith, name='sith'),
    path('list_of_recruits/<int:sith_id>', views.list_of_recruits, name='list_of_recruits'),
    path('list_of_recruits/<int:sith_id>/<int:recruit_id>', views.recruit_detail, name='recruit_detail'),
    path('accepted/<int:sith_id>/<int:recruit_id>/', views.accepted, name='accepted'),
]
