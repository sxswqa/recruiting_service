from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import RecruitForm, PollForm
from .models import Planet, Question, Recruit, Sith, Result
from django.forms import formset_factory, modelformset_factory
from django.core.mail import send_mail


# Create your views here.

def index(request):
    return render(request, 'service/index.html')


def add_recruit(request):
    if request.method != 'POST':
        form = RecruitForm()
    else:
        form = RecruitForm(data=request.POST)
        if form.is_valid():
            recruit = form.save()
            recruit.refresh_from_db()
            recruit.save()
            # return HttpResponseRedirect(reverse('service:test'))
            return HttpResponseRedirect(reverse('service:test', kwargs={"recruit_id": recruit.id}))
    context = {'form': form}
    return render(request, 'service/recruitform.html', context)


def test_recruit(request, recruit_id):
    recruit = Recruit.objects.get(id=recruit_id)
    questions = Question.objects.all()
    PollFormSet = formset_factory(form=PollForm, extra=3)
    if request.method != 'POST':
        formset = PollFormSet()
    else:
        formset = PollFormSet(request.POST)
        if formset.is_valid():
            for form in formset:
                test_results = form.save(commit=False)
                test_results.recruit = recruit
                test_results.save()
            return HttpResponseRedirect(reverse('service:index'))
    context = {'formset': formset, 'questions': questions}
    return render(request, "service/testform.html", context)


def list_sith(request):
    siths = Sith.objects.all()
    context = {'siths': siths}
    return render(request, 'service/sith.html', context)


def list_of_recruits(request, sith_id):
    sith = Sith.objects.get(id=sith_id)
    recruits = Recruit.objects.filter(accepted=False, planet_of_living=sith.planet_of_teaching)
    context = {'recruits': recruits, 'sith_id': sith_id}
    return render(request, 'service/list_of_recruits.html', context)


def recruit_detail(request, sith_id, recruit_id):
    recruit = Recruit.objects.get(id=recruit_id)
    results = Result.objects.filter(recruit_id=recruit_id)
    context = {'recruit': recruit, 'results': results, 'sith_id': sith_id}
    return render(request, 'service/recruit_detail.html', context)


def accepted(request, sith_id, recruit_id):
    recruit = Recruit.objects.get(id=recruit_id)
    sith = Sith.objects.get(id=sith_id)
    if sith.recruit_set.count() <= 3:
        recruit.accepted = True
        recruit.sith = sith
        recruit.save()
        send_mail(
            'Вы приняты!',
            'дада вы приняты',
            'be11a@yandex.ru',
            [recruit.email]
        )
        return HttpResponseRedirect(reverse('service:list_of_recruits', kwargs={"sith_id": sith_id}))
    elif sith.recruit_set.count() > 3:
        messages.error(request, "У вас не может быть больше 3 рук Тени")
        return recruit_detail(request, sith_id, recruit_id)