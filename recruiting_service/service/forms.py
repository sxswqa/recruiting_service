from django import forms

from .models import Recruit, Planet, Result


class RecruitForm(forms.ModelForm):
    name = forms.CharField(max_length=200, label='Имя')
    planet_of_living = forms.ModelChoiceField(queryset=Planet.objects.all(), label='Планета проживания')
    age = forms.IntegerField(label='Возраст')
    email = forms.EmailField(label='Email')

    class Meta:
        model = Recruit
        fields = ['name', 'planet_of_living', 'age', 'email']


CHOICES = [('True', 'Да'), ('False', 'Нет')]


class PollForm(forms.ModelForm):
    result = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES, label='Ответ')

    class Meta:
        model = Result
        fields = ['question', 'result']
        labels = {
            'question': 'Вопрос',
        }




