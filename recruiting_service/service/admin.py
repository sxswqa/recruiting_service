from django.contrib import admin

# Register your models here.
from .models import Sith, Planet, Question, Recruit, Result

admin.site.register(Sith)
admin.site.register(Planet)
admin.site.register(Question)

# не забудь убрать
admin.site.register(Recruit)
admin.site.register(Result)

